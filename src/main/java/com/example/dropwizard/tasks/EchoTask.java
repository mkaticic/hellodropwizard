package com.example.dropwizard.tasks;

import com.google.common.collect.ImmutableMultimap;
import io.dropwizard.servlets.tasks.PostBodyTask;

import java.io.PrintWriter;

public class EchoTask extends PostBodyTask {
    public EchoTask() {
        super("echo");
    }

    /**
     *
     * @param parameters - ImmutableMultimap from com.google.guava
     * @param postBody
     * @param output
     * @throws Exception
     */
    @Override
    public void execute(ImmutableMultimap<String, String> parameters, String postBody, PrintWriter output) throws Exception {
        output.write(postBody);
        output.flush();
    }
}
