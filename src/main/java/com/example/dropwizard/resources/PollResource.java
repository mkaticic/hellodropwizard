package com.example.dropwizard.resources;

import com.example.dropwizard.model.dao.jdbi.PollDAO;
import io.dropwizard.jersey.params.LongParam;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

@Path("/polls")
public class PollResource {

    private PollDAO poll;

    public PollResource(PollDAO dao) {
        poll = dao;
    }

    @Path("/{id}")
    @GET
    public String pollDetail(@PathParam("id") LongParam id){
        return poll.findQuestionById(id.get());
    }

}
