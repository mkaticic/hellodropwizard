package com.example.dropwizard.resources.ext.api;

import com.example.dropwizard.model.entities.ext.Post;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/posts")
@Consumes({"application/json"})
@Produces({"application/json"})
public interface PostsResource {

    @Consumes({"application/json"})
    @Produces({"application/json"})
    @POST
    public Post createPost(Post post);
}
