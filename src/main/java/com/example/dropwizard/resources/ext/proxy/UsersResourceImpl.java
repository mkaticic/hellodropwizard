package com.example.dropwizard.resources.ext.proxy;

import com.example.dropwizard.model.entities.ext.User;
import com.example.dropwizard.resources.ext.api.UsersResource;
import io.dropwizard.jersey.params.LongParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.List;

public class UsersResourceImpl implements UsersResource{

    private static final Logger log = LoggerFactory.getLogger(UsersResourceImpl.class);
    private static final String url = "https://jsonplaceholder.typicode.com";

    private Client client;

    public UsersResourceImpl(Client client) {
        this.client = client;
    }

    public void listUsers(){
        try{
            getUser().stream().forEach(u -> System.out.println(u.getName()));
        }catch (Throwable t){
            log.error("listUsers", t);
        }
    }

    public User getUser(String id){
        return getUser(new LongParam(id));
    }

    @Override
    public List<User> getUser() {
        List<User> users = null;

        try{
            WebTarget target = client.target(url + "/users");
            Response response = target.request().buildGet().invoke();
            users = response.readEntity(new GenericType<List<User>>() {});
        }catch (Throwable t){
            log.error("listUsers", t);
        }

        return users;
    }

    @Override
    public User getUser(LongParam id) {
        User user = null;
        try{
            WebTarget target = client.target(url + "/users/" + id);
            Response response = target.request().buildGet().invoke();
            user = response.readEntity(User.class);
        }catch (Throwable t){
            log.error("getUser", t);
        }
        return user;
    }
}
