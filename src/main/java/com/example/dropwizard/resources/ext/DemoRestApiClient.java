package com.example.dropwizard.resources.ext;

import com.example.dropwizard.model.entities.ext.Post;
import com.example.dropwizard.model.entities.ext.User;
import com.example.dropwizard.resources.ext.api.PostsResource;
import com.example.dropwizard.resources.ext.api.UsersResource;
import io.dropwizard.jersey.params.LongParam;
import org.glassfish.jersey.client.proxy.WebResourceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import java.util.List;

/**
 * Demonstrates WebResourceFactory usage for generating resource from interface
 */
public class DemoRestApiClient {

    private static final Logger log = LoggerFactory.getLogger(DemoRestApiClient.class);

    private final Client client;
    private final PostsResource postsResouce;
    private final UsersResource usersResouce;

    public DemoRestApiClient(Client client) {
        this.client = client;
        WebTarget wt = client.target("https://jsonplaceholder.typicode.com");
        this.postsResouce = WebResourceFactory.newResource(PostsResource.class, wt);
        this.usersResouce = WebResourceFactory.newResource(UsersResource.class, wt);
    }



    public Post createPost(Post post){
        try{
            post = postsResouce.createPost(post);
        }catch (Throwable t){
            log.error("createDummyPost", t);
        }
        return post;
    }

    public Post createDummyPost(){
        Post post = Post.PostBuilder.aPost().build();
        createPost(post);
        return post;
    }

    public List<User> listUsers(){
        try{
            return usersResouce.getUser();
        }catch (Throwable t){
            log.error("listUsers", t);
        }
        return  null;
    }

    public User getUser(String id){
        User user = null;
        try{
            return usersResouce.getUser(new LongParam(id));
        }catch (Throwable t){
            log.error("getUser", t);
        }
        return user;
    }

}
