package com.example.dropwizard.resources.ext.api;

import com.example.dropwizard.model.entities.ext.User;
import io.dropwizard.jersey.params.LongParam;

import javax.ws.rs.*;
import java.util.List;

@Path("/users")
@Consumes({"application/json"})
@Produces({"application/json"})
public interface UsersResource {

    @GET
    public List<User> getUser();

    @GET
    @Path("/{id}")
    public User getUser(@PathParam("id") LongParam id);

}
