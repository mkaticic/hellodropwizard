package com.example.dropwizard.resources.ext.proxy;

import com.example.dropwizard.model.entities.ext.Post;
import com.example.dropwizard.resources.ext.api.PostsResource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/posts")
@Produces(MediaType.APPLICATION_JSON)
public class PostsResourceImpl implements PostsResource {

    private static final Logger log = LoggerFactory.getLogger(PostsResourceImpl.class);
    private static final String url = "https://jsonplaceholder.typicode.com";

    private Client client;

    public PostsResourceImpl(Client client) {
        this.client = client;
    }

    public Post createDummyPost(){
        Post post = Post.PostBuilder.aPost().build();
        return createPost();
    }

    @POST
    @Path("createDummyPost")
    @Produces(MediaType.APPLICATION_JSON)
    public Post createPost(){
        Post p = createDummyPost();
        return p;
    }

    @Override
    public Post createPost(Post post) {
        try{
            WebTarget target = client.target(url + "/posts");
            Entity postEnt = Entity.entity(post, MediaType.APPLICATION_JSON);
            Response response = target.request().buildPost(postEnt).invoke();
            post = response.readEntity(Post.class);
        }catch (Throwable t){
            log.error("createDummyPost", t);
        }
        return post;
    }
}
