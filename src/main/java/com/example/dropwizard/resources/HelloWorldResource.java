package com.example.dropwizard.resources;

import com.codahale.metrics.annotation.Timed;
import com.example.dropwizard.api.Saying;
import com.example.dropwizard.model.entities.ext.Post;
import com.example.dropwizard.model.entities.ext.User;
import com.example.dropwizard.resources.ext.DemoRestApiClient;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by mirna on 10/2/17.
 */
@Api("/hello-world")
@Path("/hello-world")
@Produces(MediaType.APPLICATION_JSON)
public class HelloWorldResource {

    private final String template;
    private final String defaultName;
    private final AtomicLong counter;

    private DemoRestApiClient extApiClient;

    public HelloWorldResource(DemoRestApiClient extApiClient, String template, String defaultName) {
        this.template = template;
        this.defaultName = defaultName;
        this.counter = new AtomicLong();
        this.extApiClient = extApiClient;
    }

    @ApiOperation(
            value = "Get hello object",
            notes = "Say hello to user in template defined in configuratio file.",
            response = Saying.class
    )
    @GET
    @Timed
    public Saying sayHello(
            @QueryParam("name")
            @ApiParam(value = "Name of the visitor", defaultValue = "Mirna")
            Optional<String> name

    ) {

        final String value = String.format(template, name.orElse(defaultName));
        return new Saying(counter.incrementAndGet(), value);
    }

    @GET
    @Timed
    @Path("advanced")
    public Saying sayHelloById(@QueryParam("id") Optional<String> id) {

        User user;
        if(id.isPresent()) {
            user = extApiClient.getUser(id.get());
            final String value = String.format(template, user.getName());
            return new Saying(Long.valueOf(user.getId()), value);
        }

        return new Saying(counter.incrementAndGet(), defaultName);
    }

    @GET
    @Path("iso8601Date")
    public String iso8601Date(){
        String pattern = "YYYY-MM-DD'T'HH:MM:SSZ";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        String date = sdf.format(new Date());
        return date + " (pattern: " + pattern + ")";
    }

    @GET
    @Path("users")
    public List<User> listUsers(){
        return extApiClient.listUsers();
    }

    @POST
    @Path("createDummyPost")
    @Produces(MediaType.APPLICATION_JSON)
    public Post createDummyPost(){
        Post p = extApiClient.createDummyPost();
        return p;
    }

    @POST
    @Path("createPost")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Post createPost(
            @NotNull
            @ApiParam(
                    value = "Post data"
            )
            Post post

    ){
        Post p = extApiClient.createPost(post);
        return p;
    }

}