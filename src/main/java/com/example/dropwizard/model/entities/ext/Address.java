package com.example.dropwizard.model.entities.ext;

import java.util.HashMap;

public class Address{

    private String street, suite, city, zipcode;
    private HashMap<String, String> geo;

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getSuite() {
        return suite;
    }

    public void setSuite(String suite) {
        this.suite = suite;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public HashMap<String, String> getGeo() {
        return geo;
    }

    public void setGeo(HashMap<String, String> geo) {
        this.geo = geo;
    }
}