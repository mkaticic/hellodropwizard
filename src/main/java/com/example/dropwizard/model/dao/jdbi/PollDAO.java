package com.example.dropwizard.model.dao.jdbi;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;

public interface PollDAO {

    // @SqlUpdate("create table something (id int primary key, name varchar(100))")
    // void createSomethingTable();

    @SqlUpdate("insert into polls_poll (id, question) values (:id, :question)")
    void insert(@Bind("id") int id, @Bind("question") String name);

    @SqlQuery("select question from polls_poll where id = :id")
    String findQuestionById(@Bind("id") Long id);
}
