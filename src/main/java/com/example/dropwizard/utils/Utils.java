package com.example.dropwizard.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Utils {

    public static String readSwaggerRes(String filename){
        String path = "/resources/swagger/" + filename;
        String res = "";
        try {
            res = new String(Files.readAllBytes(Paths.get(filename)));
        } catch (IOException e) {
            // noop
        }
        return res;
    }

}
