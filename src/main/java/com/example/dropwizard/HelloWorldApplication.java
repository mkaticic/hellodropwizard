package com.example.dropwizard;

import com.example.dropwizard.health.TemplateHealthCheck;
import com.example.dropwizard.lifecycle.LifecycleNotifier;
import com.example.dropwizard.model.dao.jdbi.PollDAO;
import com.example.dropwizard.resources.HelloWorldResource;
import com.example.dropwizard.resources.PollResource;
import com.example.dropwizard.resources.ext.DemoRestApiClient;
import com.example.dropwizard.resources.ext.proxy.PostsResourceImpl;
import com.example.dropwizard.tasks.EchoTask;
import io.dropwizard.Application;
import io.dropwizard.client.JerseyClientBuilder;
import io.dropwizard.jdbi.DBIFactory;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.federecio.dropwizard.swagger.SwaggerBundle;
import io.federecio.dropwizard.swagger.SwaggerBundleConfiguration;
import org.skife.jdbi.v2.DBI;

import javax.ws.rs.client.Client;

/**
 * curl --request POST --header 'content-type:application/json'  --data '{"name": "mirna"}'  --url http://localhost:8081/tasks/echo
 *
 * Created by mirna on 10/2/17.
 */
public class HelloWorldApplication extends Application<HelloWorldConfiguration> {

    public static void main(String[] args) throws Exception {
        new HelloWorldApplication().run(args);
    }

    @Override
    public String getName() {
        return "hello-world";
    }

    /**
     * <p>
     * Before a Dropwizard application can provide the command-line interface,
     * parse a configuration file, or run as a server,
     * it must first go through a <b>bootstrapping phase</b>.
     * </p>
     * <p>
     * This phase corresponds to your Application subclass’s initialize method.
     * You can add <i>Bundles, Commands, or register Jackson modules</i>
     * to allow you to include custom types as part of your configuration class
     * <p>
     * @param bootstrap
     */
    @Override
    public void initialize(Bootstrap<HelloWorldConfiguration> bootstrap) {
        // bundles, commands, jackson modules

        bootstrap.addBundle(new SwaggerBundle<HelloWorldConfiguration>() {
            @Override
            protected SwaggerBundleConfiguration getSwaggerBundleConfiguration(HelloWorldConfiguration configuration) {
                return configuration.swaggerBundleConfiguration;
            }
        });
    }

    /**
     * A <b>Dropwizard Environment</b> consists of all the
     * <i>Resources, servlets, filters, Health Checks, Jersey providers, Managed Objects, Tasks, and Jersey properties</i>
     * which your application provides.
     *
     * @param config
     * @param environment
     */
    @Override
    public void run(HelloWorldConfiguration config, Environment environment) {

        final DBIFactory factory = new DBIFactory();
        final DBI jdbi = factory.build(environment, config.getDataSourceFactory(), "sqllite3");
        final PollDAO dao = jdbi.onDemand(PollDAO.class);

        final TemplateHealthCheck healthCheck = new TemplateHealthCheck(config.getTemplate());

        final Client client = new JerseyClientBuilder(environment)
                .using(config.getJerseyClientConfiguration())
                .build(getName());

        // ext client
        final DemoRestApiClient extRestClient = new DemoRestApiClient(client);
        final PostsResourceImpl postsResource = new PostsResourceImpl(client);

        final HelloWorldResource hello = new HelloWorldResource(
                extRestClient,
                config.getTemplate(),
                config.getDefaultName()
        );

        // healthchecks
        environment.healthChecks().register("template", healthCheck);

        // tasks
        environment.admin().addTask(new EchoTask()); // http://localhost:8081/tasks/echo

        // resources
        environment.jersey().register(hello);
        environment.jersey().register(new PollResource(dao));
        environment.jersey().register(postsResource);

        // managed objects
        environment.lifecycle().manage(new LifecycleNotifier());
    }

}
