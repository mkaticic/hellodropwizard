package com.example.dropwizard.lifecycle;

import io.dropwizard.lifecycle.Managed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDate;

public class LifecycleNotifier implements Managed{

    private static final Logger log = LoggerFactory.getLogger(LifecycleNotifier.class);

    @Override
    public void start() throws Exception {
        log.info("*********************Application started at " + LocalDate.now());
    }

    @Override
    public void stop() throws Exception {
        log.info("*********************Application stopped at " + LocalDate.now());
    }
}
