import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import java.sql.Driver;

public class HelloOracleApp extends Application<HelloOracleConf> {

    public static void main(String[] args) throws Exception {
        new HelloOracleApp().run(args);
    }

    @Override
    public void initialize(Bootstrap<HelloOracleConf> bootstrap) {
        bootstrap.addBundle(new MigrationsBundle<HelloOracleConf>() {
            @Override
            public DataSourceFactory getDataSourceFactory(HelloOracleConf configuration) {
                return configuration.getDataSourceFactory();
            }
        });
    }

    @Override
    public void run(HelloOracleConf helloOracleConf, Environment environment) throws Exception {

    }
}
