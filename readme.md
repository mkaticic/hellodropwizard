
##Resources
###hello-world
    - http://localhost:8080/hello-world/name=Mirna 
    - http://localhost:8080/hello-world/iso8601Date
        - print current date and time in iso 8601 format

###poll resource example
    http://localhost:8080/polls/1

###admin port
    http://localhost:8081/healthcheck
 
 
### jersey client
#### GET
    - http://localhost:8080/hello-world/advanced?id=7
        - User info retrieved through DemoRestApiClient which uses WebResourceFactory
        - WebResourceFactory generate resource implementations based on given interface 
        - interface definitions: com.example.dropwizard.resources.ext.api package
        - to build the client we are using JerseyClientBuilder from io.dropwizard.client module
#### POST
    - curl --request POST http://localhost:8080/posts/createDummyPost
        - uses explicit resource interface implementation


##Tasks
###task example
    curl --request POST --header 'content-type:application/json'  --data '{"name": "mirna"}'  --url http://localhost:8081/tasks/echo


## Swagger configuration
Application uses dropwizard-swagger dependency which provides OpenAPI Specification generation tools and SwaggerUI resource. 
OpenAPI Specification is specification for machine-readable interface files for describing, producing, consuming, and visualizing RESTful Web services.[1] A variety of tools can generate code, documentation and test cases given an interface file
#### dropwizard-swagger mvn dependency
  - HelloWorldApplication.initialize: SwaggerBundle added in dropwizard bootstrap
  - HelloWorldConfiguration: added SwaggerBundleConfiguration property
  - HelloWorldResource: added io.swagger.annotations (@Api, @ApiOperation etc.)
#### SwaggerBundle provides: 
##### SwaggerUI runtime documentation based on annotations at
    - http://localhost:8080/swagger
##### OpenAPI Specification in json and yaml format reachable on following links 
    - http://localhost:8080/swagger.json
    - http://localhost:8080/swagger.yaml
  
## DB migrations: hello-oracle module
    - java -Ddw.database.user=bla -Ddw.database.password=bla -jar target/hello-oracle-1.0-SNAPSHOT.jar db dump hello-oracle.yml > src/main/resources/migrations.xml
    - java -Ddw.database.user=bla -Ddw.database.password=bla -jar target/hello-oracle-1.0-SNAPSHOT.jar db generate-docs --migrations src/main/resources/migrations.xml hello-oracle.yml ~/db-docs/