package com.example.pizza_guice.app;

import com.example.pizza_guice.api.*;
import com.example.pizza_guice.app.credit_card.FakeCreditCardProcessor;
import com.example.pizza_guice.app.log.InMemoryTransactionLog;
import com.example.pizza_guice.app.model.CreditCardImpl;
import com.example.pizza_guice.app.model.PizzaOrderImpl;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

public class BillingServiceImplTest {

    private final PizzaOrder order = new PizzaOrderImpl(100);
    private final CreditCard creditCard = new CreditCardImpl("1234", 11, 2010);

    @Inject
    private TransactionLog transactionLog;

    @Inject
    private CreditCardProcessor processor;

    private class InjectionConf extends AbstractModule {

        @Override
        protected void configure() {
            bind(TransactionLog.class).to(InMemoryTransactionLog.class);
            bind(CreditCardProcessor.class).to(FakeCreditCardProcessor.class);
            bind(BillingService.class).to(BillingServiceImpl.class);
        }

    }

    @Test
    public void testSuccessfulCharge() {

        Injector injector = Guice.createInjector(new InjectionConf());
        BillingService billingService = injector.getInstance(BillingService.class);
        Receipt receipt = billingService.chargeOrder(order, creditCard);

        assertTrue(receipt.hasSuccessfulCharge());
        assertEquals(100, receipt.getAmountOfCharge());
        assertTrue(transactionLog.wasSuccessLogged());
    }
}