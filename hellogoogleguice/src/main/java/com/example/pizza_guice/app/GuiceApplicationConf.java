package com.example.pizza_guice.app;

import io.dropwizard.Configuration;

public class GuiceApplicationConf extends Configuration {
    Integer pizzaCount;

    public Integer getPizzaCount() {
        return pizzaCount;
    }

    public void setPizzaCount(Integer pizzaCount) {
        this.pizzaCount = pizzaCount;
    }
}
