package com.example.pizza_guice.app;

import com.example.pizza_guice.api.BillingService;
import com.example.pizza_guice.api.CreditCard;
import com.example.pizza_guice.api.PizzaOrder;
import com.example.pizza_guice.api.Receipt;
import com.example.pizza_guice.app.model.CreditCardImpl;
import com.example.pizza_guice.app.model.PizzaOrderImpl;
import com.google.inject.Guice;
import com.google.inject.Injector;
import io.dropwizard.Application;
import io.dropwizard.Configuration;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GuiceApplication extends Application<GuiceApplicationConf>{

    private static final Logger log = LoggerFactory.getLogger(GuiceApplication.class);

    public static void main(String[] args) throws Exception {
        new GuiceApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<GuiceApplicationConf> bootstrap) {
        // bundles, commands, jackson modules
    }

    @Override
    public void run(GuiceApplicationConf configuration, Environment environment) throws Exception {
        Injector injector = Guice.createInjector(new BillingInjectionConf());
        BillingService billingService = injector.getInstance(BillingService.class);

        PizzaOrder pizzaOrder = new PizzaOrderImpl(configuration.getPizzaCount());
        CreditCard cn = new CreditCardImpl("1234", 12, 2017);

        Receipt receipt = billingService.chargeOrder(pizzaOrder, cn);

        receipt.print();


    }
}
