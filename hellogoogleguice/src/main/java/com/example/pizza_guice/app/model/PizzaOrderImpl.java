package com.example.pizza_guice.app.model;

import com.example.pizza_guice.api.PizzaOrder;

import java.math.BigDecimal;

public class PizzaOrderImpl implements PizzaOrder {

    int count;

    public PizzaOrderImpl(int count) {
        this.count = count;
    }

    @Override
    public BigDecimal getAmount() {
        return new BigDecimal(count * 40);
    }
}
