package com.example.pizza_guice.app;

import com.example.pizza_guice.api.BillingService;
import com.example.pizza_guice.api.CreditCardProcessor;
import com.example.pizza_guice.api.TransactionLog;
import com.example.pizza_guice.app.credit_card.PaypalCreditCardProcessor;
import com.example.pizza_guice.app.log.DatabaseTransactionLog;
import com.google.inject.AbstractModule;

public class BillingInjectionConf extends AbstractModule {

    @Override
    protected void configure() {
        bind(TransactionLog.class).to(DatabaseTransactionLog.class);
        bind(CreditCardProcessor.class).to(PaypalCreditCardProcessor.class);
        bind(BillingService.class).to(BillingServiceImpl.class);
    }

}