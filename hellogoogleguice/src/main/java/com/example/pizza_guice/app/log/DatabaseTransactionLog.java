package com.example.pizza_guice.app.log;

import com.example.pizza_guice.api.ChargeResult;
import com.example.pizza_guice.api.TransactionLog;
import com.example.pizza_guice.api.UnreachableException;

public class DatabaseTransactionLog implements TransactionLog{
    @Override
    public void logChargeResult(ChargeResult result) {

    }

    @Override
    public void logConnectException(UnreachableException e) {

    }

    @Override
    public boolean wasSuccessLogged() {
        return false;
    }
}
