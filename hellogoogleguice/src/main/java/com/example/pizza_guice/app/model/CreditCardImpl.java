package com.example.pizza_guice.app.model;

import com.example.pizza_guice.api.CreditCard;

public class CreditCardImpl implements CreditCard {

    private final String cardNumber;
    private final int month;
    private final int year;

    public CreditCardImpl(String cardNumber, int month, int year) {
        this.cardNumber = cardNumber;
        this.month = month;
        this.year = year;
    }

}
