package com.example.pizza_guice.app.credit_card;

import com.example.pizza_guice.api.ChargeResult;
import com.example.pizza_guice.api.CreditCard;
import com.example.pizza_guice.api.CreditCardProcessor;
import com.example.pizza_guice.api.UnreachableException;

import java.math.BigDecimal;

public class FakeCreditCardProcessor implements CreditCardProcessor {
    private CreditCard creditCard;
    private BigDecimal amount;

    @Override
    public ChargeResult charge(CreditCard creditCard, BigDecimal amount) throws UnreachableException {

        this.creditCard = creditCard;
        this.amount = amount;

        return new ChargeResult(true, null);
    }

    public CreditCard getCardOfOnlyCharge() {
        return null;
    }

    public BigDecimal getAmountOfOnlyCharge() {
        return amount;
    }
}
