package com.example.pizza_guice.app.log;

import com.example.pizza_guice.api.TransactionLog;

public class TransactionLogFactory {

    private static TransactionLog log;

    public static TransactionLog getLog() {
        return log;
    }

    public static void setInstance(TransactionLog log) {
        TransactionLogFactory.log = log;
    }

    public static TransactionLog getInstance(){
        if(log == null){
            log = new DatabaseTransactionLog();
        }
        return log;
    }

}
