package com.example.pizza_guice.app.credit_card;

import com.example.pizza_guice.api.ChargeResult;
import com.example.pizza_guice.api.CreditCard;
import com.example.pizza_guice.api.CreditCardProcessor;
import com.example.pizza_guice.api.UnreachableException;

import java.math.BigDecimal;

public class SquareCreditCardProcessor implements CreditCardProcessor {
    @Override
    public ChargeResult charge(CreditCard creditCard, BigDecimal amount) throws UnreachableException {
        return null;
    }
}
