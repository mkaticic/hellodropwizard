package com.example.pizza_guice.api;

import java.math.BigDecimal;

public interface CreditCardProcessor {
    ChargeResult charge(CreditCard creditCard, BigDecimal amount) throws UnreachableException;
}
