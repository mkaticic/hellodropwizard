package com.example.pizza_guice.api;

public class ChargeResult {

    private boolean wasSuccessful = false;
    private String declineMessage;

    public ChargeResult(boolean wasSuccessful, String declineMessage) {
        this.wasSuccessful = wasSuccessful;
        this.declineMessage = declineMessage;
    }

    public boolean wasSuccessful() {
        return wasSuccessful;
    }

    public String getDeclineMessage() {
        return declineMessage;
    }
}
