package com.example.pizza_guice.api;

import java.math.BigDecimal;

public interface PizzaOrder {
    BigDecimal getAmount();
}
