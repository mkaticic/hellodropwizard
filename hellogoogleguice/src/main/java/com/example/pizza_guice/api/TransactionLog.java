package com.example.pizza_guice.api;

public interface TransactionLog {
    void logChargeResult(ChargeResult result);

    void logConnectException(UnreachableException e);

    boolean wasSuccessLogged();
}
