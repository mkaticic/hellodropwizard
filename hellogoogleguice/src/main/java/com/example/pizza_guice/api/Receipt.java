package com.example.pizza_guice.api;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

public class Receipt {

    private static final Logger log = LoggerFactory.getLogger(Receipt.class);

    private final boolean successfulCharge;
    private final String message;
    private final BigDecimal amountOfCharge;

    public Receipt(boolean successfulCharge, BigDecimal amountOfCharge) {
        this.successfulCharge = successfulCharge;
        this.amountOfCharge = amountOfCharge;
        message = "charge was successful";
    }

    public Receipt(boolean successfulCharge, String message) {
        this.successfulCharge = successfulCharge;
        this.message = message;
        this.amountOfCharge = BigDecimal.ZERO;
    }

    public static Receipt forDeclinedCharge(String declineMessage) {
        return new Receipt(false, declineMessage);
    }

    public static Receipt forSuccessfulCharge(BigDecimal amount) {
        return new Receipt(true, amount);
    }

    public static Receipt forSystemFailure(String message) {
        return new Receipt(false, message);
    }

    public boolean hasSuccessfulCharge(){
        return successfulCharge;
    }

    public BigDecimal getAmountOfCharge(){
        return  amountOfCharge;
    }

    public void print(){
        if(successfulCharge) {
            log.info("amount: " + amountOfCharge);
        }else{
            log.info("charge failed: " + message);
        }
    }
}
